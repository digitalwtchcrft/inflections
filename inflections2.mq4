//+------------------------------------------------------------------+
//|                                                 inflections2.mq4 |
//|                                                     Alex Herrera |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property strict

#property indicator_color1 clrOrangeRed
#property indicator_color2 clrForestGreen
//--- indicator buffers
double         NegMarkBuffer[];
double         PosMarkBuffer[];
double         MovAvBuffer[];

//-- input parameters
input int MA_Period = 20; // Moving Average Period
input bool MarkType = 0;  // Marking Type (0 - P/T  1 - Inflections)
//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
  {
//--- indicator buffers mapping
   SetIndexBuffer(0,NegMarkBuffer);
   SetIndexBuffer(1,PosMarkBuffer);
   SetIndexBuffer(2,MovAvBuffer);
//--- setting a code from the Wingdings charset as the property of PLOT_ARROW
  // PlotIndexSetInteger(0,PLOT_ARROW,234);
 //  PlotIndexSetInteger(1,PLOT_ARROW,233);
   
   SetIndexStyle(0,DRAW_ARROW);
   SetIndexArrow(0,234);
   SetIndexStyle(1,DRAW_ARROW);
   SetIndexArrow(1,233);
   
   SetIndexStyle (2,DRAW_LINE,STYLE_DOT,3);

//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
  {

// return(rates_total);
//---


   int numbars = iBars(NULL, 0);

// PrintFormat("OnCalc: iBars: [%d] vs. Bars: [%d]", numbars, Bars);


   for(int i = (numbars - 1); i >= 0; i--)
     {

      double sma = simpleMA(i, MA_Period, close);
      MovAvBuffer[i] = sma;

      int sma_pos_1 = i + 2;
      int sma_pos_2 = i + 1;
      int sma_pos_11 = i + 3;

      if(sma_pos_11 < numbars)
        {
         double delta_1_2 = MovAvBuffer[sma_pos_2] - MovAvBuffer[sma_pos_1];
         double delta_2_i = MovAvBuffer[i] - MovAvBuffer[sma_pos_2];

         double delta_11_1 = MovAvBuffer[sma_pos_1] - MovAvBuffer[sma_pos_11];

         double delta_0 = delta_1_2 - delta_11_1;
         double delta_1 = delta_2_i - delta_1_2;

         // START INFLECTIONS
         if(MarkType == 1)
           {
            if(delta_0 > 0.0)
              {
               if(delta_1 <= 0.0)
                 {
                  NegMarkBuffer[sma_pos_2] = MovAvBuffer[sma_pos_2] - 0.001;
                 }
              }
            else
               if(delta_0 < 0)
                 {
                  if(delta_1 >= 0.0)
                    {
                     PosMarkBuffer[sma_pos_2] = MovAvBuffer[sma_pos_2] + 0.001;
                    }
                 }
               else
                 {
                  if(delta_1 > 0.0)
                    {
                     PosMarkBuffer[sma_pos_2] = MovAvBuffer[sma_pos_2] + 0.001;
                    }
                  else
                     if(delta_1 < 0.0)
                       {
                        NegMarkBuffer[sma_pos_2] = MovAvBuffer[sma_pos_2] - 0.001;
                       }
                 }

           }


         //END INFLECTIONS


         // START PEAKS/TROUGHS
         else
           {

            if(delta_1_2 > 0.0)
              {
               if(delta_2_i <= 0.0)
                 {
                  NegMarkBuffer[sma_pos_2] = MovAvBuffer[sma_pos_2] - 0.0005;
                 }
              }
            else
               if(delta_1_2 < 0)
                 {
                  if(delta_2_i >= 0.0)
                    {
                     PosMarkBuffer[sma_pos_2] = MovAvBuffer[sma_pos_2] + 0.0005;
                    }
                 }
               else
                 {
                  if(delta_2_i > 0.0)
                    {
                     PosMarkBuffer[sma_pos_2] = MovAvBuffer[sma_pos_2] + 0.0005;
                    }
                  else
                     if(delta_2_i < 0.0)
                       {
                        NegMarkBuffer[sma_pos_2] = MovAvBuffer[sma_pos_2] - 0.0005;
                       }
                 }

           }
        }

     }
//END PEAKS/TROUGHS

//--- return value of prev_calculated for next call
   return(rates_total);
  }


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double simpleMA(const int position, const int period, const double &price[])
  {
   double result = 0.0;


   if((position + period) < Bars)
     {
      for(int i = (position + period - 1); i >= position; i--)
        {
         result += price[i];
        }

      result /= period;
     }

   return result;
  }
//+------------------------------------------------------------------+
